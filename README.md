## Tips & tricks

For `py-stackexchange` to work on Python 3, installing from Github is required,
even if PiPy version is the same (2.2.6).

```shell
pip install --upgrade https://github.com/lucjon/Py-StackExchange/archive/master.zip
```
