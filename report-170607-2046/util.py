#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def name(id_, graph):
    """Gived _id, returns the name of Stack Overflow user into graph.

    Args:
        id_ (int): id into graph.
        graph (networkx.Graph): graph to search into.

    Returns:
        Name for user id provided.
    
    """
    return graph.node[id_]['name']

def inv_dict(d):
    """Invert a dict biunivocally putting duplicated values into a list.

    Args:
        d (dict): dictionary to invert.

    Returns:
        Inverted dictionary, with duplicates in a list.
    
    """
    inv_map = {}
    for k,v in d.items():
        inv_map[v] = inv_map.get(v, [])
        inv_map[v].append(k)
    return inv_map

def sort_key(d, reverse=False):
    """Sort a dictionary by key.

    Args:
        d (dict): dictionary to sort.
        reverse (bool): wheter to reverse sorting.

    Returns:
        Dictionary sorted by key (optionally reversed).
    
    """
    return dict(sorted(d.items(), reverse=reverse))

def correlation(x, y):
    """A simple correlation index for two lists.

    Args:
        x (list): a list of items.
        y (list): a list of items.

    Returns:
        Correlation index calulated as #equals/#elements.
    
    """
    sum_ = 0
    for i,j in zip(x, y):
        if i == j:
            sum_ += 1
    return sum_ / float(len(x))
