#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, pickle
from collections import deque
from time import sleep
from os import path

import requests
import networkx as nx

# define api key e set user agent not to trigger blacklist
API_KEY = 'ekaG*7C8*io7TlLGtFUFRw(('
API_URL = 'http://api.stackexchange.com/2.2'
UA = { 'User-Agent': 'StackOverflow App v0.1.0' }

# global variables to hold state data
GRAPH = None
NODEQ = None
PAGE = None

# define save folder, create it and define paths for state data files
folder = 'saves'
os.makedirs(folder, exist_ok=True)
g_fn = path.join(folder, 'graph.pkl')
n_fn = path.join(folder, 'node_queue.pkl')
p_fn = path.join(folder, 'page.pkl')

def req(t, url, ids, params, page=None):
    '''Make a request to Stack Overflow API, providing debug data and returning
    json response.

    Args:
        t (str): type of request.
        url (str): API endpoint url.
        ids (str): id to send API request for.
        params (dict): request query parameters.
        page (int): page number to request.

    Returns:
        Json parsed response (as dict or list).
    '''
    if page:
        params['page'] = page
    url = url.format(ids=ids)
    r = requests.get(API_URL+url, headers=UA, params=params)
    if t == 'u':
        type = 'User'
        char = '='
        rep = 20
    else:
        type = 'Question'
        char = '-'
        rep = 10
    print('{} {}: {}'.format(char * rep, type, ids))
    return r.json()

def open_data():
    '''Open state files or create them if they doesn't exist.

    '''
    global GRAPH
    global NODEQ
    global PAGE

    # open graph
    if path.isfile(g_fn):
        GRAPH = nx.read_gpickle(g_fn)
    else:
        # graph should exist in any case
        print('No graph found!')
        sys.exit(255)
    
    # open node queue
    if path.isfile(n_fn):
        with open(n_fn, 'rb') as f:
            NODEQ = pickle.load(f)
    else:
        # create it with all nodes in graph in queue
        NODEQ = deque(GRAPH.nodes())

    # open page
    if path.isfile(p_fn):
        with open(p_fn, 'rb') as f:
            PAGE = pickle.load(f)
    else:
        # start from page 0
        PAGE = 0

def save_state(graph, node, page, node_top=None, save_graph=True,
        save_nodeq=True):
    '''Save state to file as graph, node queue, current page, providing debug
    data.
    
    Args:
        graph (networkx.Graph): graph to save.
        node (collections.deque): node queue to save.
        page (int): page number to save.
        node_top (int): node to put on top of deque or None.
        save_graph (bool): whether to save graph.
        save_nodeq (bool): whether to save node queue.
    '''
    mx = \
"""
>>>>>>>>>>>>>>>>
| Nodes: {nodes}, Edges: {edges}
|
| Saving state...
""".strip().format(nodes=graph.order(), edges=graph.size())

    if save_graph and save_nodeq:
        print(mx, end='')

    # save graph
    if save_graph:
        nx.write_gpickle(graph, g_fn)
    # if node_top is defined add it to node_queue and save a copy of it
    if save_nodeq:
        if node_top:
            s_node = node.copy()
            s_node.appendleft(node_top)
        else:
            s_node = node
        with open(n_fn, 'wb') as f:
            pickle.dump(s_node, f)
    # save page
    with open(p_fn, 'wb') as f:
        pickle.dump(page, f)
    
    mx = \
"""
|
| Done.
>>>>>>>>>>>>>>>>
""".strip()

    if save_graph and save_nodeq:
        print(mx)

def get_answers(id_):
    '''Integrate graph with new question, trying to find reentrant edges from
    already present nodes answers.

    Args:
        id_ (int): id to get answers for.
    
    '''
    global GRAPH
    global NODEQ
    global PAGE

    # answer request parameters
    params = {
        'key': API_KEY,
        'pagesize': 100,
        'order': 'desc',
        'sort': 'creation',
        'site': 'stackoverflow.com',
        'filter': 'default'
    }
    url = '/users/{ids}/answers'
    
    while True:
        try:
            # print page count
            PAGE += 1
            print(f'Cycle number {PAGE}')
            
            # get json
            data = req('u', url, id_, params, PAGE)

            # print remaining quota if available
            if 'quota_remaining' in data:
                print('{} Quota: {}'.format('*'*40, data['quota_remaining']))
                if data['quota_remaining'] == 0:
                    to_exit = True
                else:
                    to_exit = False

            # sleep anyway
            sleep(0.17)

            # watch out for errors
            if 'error_id' in data:
                print(data['error_message'])
                break

            # get every answer with associated data
            for a in data['items']:
                a_id = a['answer_id']
                q_id = a['question_id']

                # retrieve questioner from question id
                q_params = {
                    'key': API_KEY,
                    'order': 'desc',
                    'sort': 'creation',
                    'site': 'stackoverflow.com',
                    'filter': 'default',
                    'body': 'true'
                }
                q_url = '/questions/{ids}'
                q_data = req('q', q_url, q_id, q_params)

                if 'quota_remaining' in q_data and data['quota_remaining'] == 0:
                    to_exit = True
                else:
                    to_exit = False

                # sleep anyway
                sleep(0.17)

                # watch out for errors
                if 'error_id' in q_data:
                    print(q_data['error_message'])
                    break

                try:
                    owner = q_data['items'][0]['owner']
                    qr_id = owner['user_id']

                    # insert into graph
                    if qr_id in GRAPH:
                        if GRAPH.has_edge(id_, qr_id):
                            GRAPH[id_][qr_id]['weight'] += 1
                        else:
                            GRAPH.add_edge(id_, qr_id, weight=1)

                        # save graph, nodeq, page
                        save_state(GRAPH, NODEQ, PAGE-1, id_)
                
                    save_state(GRAPH, NODEQ, PAGE-1, id_, False, False)
                except:
                    continue

            # must wait...
            if 'backoff' in data:
                print(f'Sleeping for {data["backoff"]} seconds.')
                sleep(data['backoff'])
            if 'backoff' in q_data:
                print(f'Sleeping for {q_data["backoff"]} seconds.')
                sleep(q_data['backoff'])

            # if no more data return list obtained
            if not data['has_more']:
                break

            if to_exit:
                print('No more api requests...')
                sys.exit(255)
        except Exception as e:
            print('Exception occurred :( {}'.format(e))
            if str(e) == 'Violation of backoff parameter':
                # in case of backoff (unknown error)
                print('Waiting', end=' ')
                for i in range(10, 0, -1):
                    print(i, end=' ')
                    sleep(1)
                PAGE -= 1
            pass

if __name__ == '__main__':
    open_data()
    
    while len(NODEQ) > 0:
        current_node = NODEQ.popleft()
        get_answers(current_node)
        
        # save state
        save_state(GRAPH, NODEQ, 0)
