#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pickle, os, sys
import collections as coll
from os import path

import networkx as nx

import poorman_so
from poorman_so import get_answers

# https://stackapps.com/apps/oauth/view/9741
API_KEY = 'ekaG*7C8*io7TlLGtFUFRw(('
poorman_so.API_KEY = API_KEY

# folder to contain saves
save_folder = 'saves'
os.makedirs(save_folder, exist_ok=True)
graph_name = 'graph.pkl'
node_name = 'node_queue.pkl'
answer_name = 'answer_queue.pkl'
g_filename = path.join(save_folder, graph_name)
n_filename = path.join(save_folder, node_name)
a_filename = path.join(save_folder, answer_name)

def error(message):
    """Print a message to stderr.

    Args:
        message (str): message to print.

    """
    print(message, file=sys.stderr)

def save_state(graph, node, answer, node_top=None):
    mx = \
"""
--------->
| Nodes: {nodes}, Edges: {edges}
|
| Saving state...
""".strip().format(nodes=graph.order(), edges=graph.size())

    print(mx, end='')

    # save graph
    nx.write_gpickle(graph, g_filename)
    # if node_top is defined add it to node_queue and save a copy of it
    if node_top:
        s_node = node.copy()
        s_node.append(node_top)
    else:
        s_node = node
    with open(n_filename, 'wb') as f:
        pickle.dump(s_node, f)
    # save answers queue
    with open(a_filename, 'wb') as f:
        pickle.dump(answer, f)
    
    mx = \
"""
|
| Done.
--------->
""".strip()
    
    print(mx)

# open saved graph if a save folder exist, else print a reminder.
if path.isfile(g_filename):
    graph = nx.read_gpickle(g_filename)
else:
    error(f'Forgot to copy {graph_name}!')
    sys.exit(255)

# open saved node queue, otherwise create it
if path.isfile(n_filename):
    with open(n_filename, 'rb') as f:
        node_q = pickle.load(f)
else:
    node_q = coll.deque(graph.nodes())

# open saved answer queue otherwise create it
if path.isfile(a_filename):
    with open(a_filename, 'rb') as f:
        answer_q = pickle.load(f)
else:
    answer_q = coll.deque()

while len(node_q) > 0:
    try:
        # retrieve last used node
        answerer = node_q.popleft()

        # if answer queue is empty or non existent, retrieve answers
        # else use already loaded answers
        if not answer_q or len(answer_q) == 0:
            # signal answer queue is created, not restored
            restored = False

            # fetch answers
            error(f'-> Fetching answer for {answerer}.')
            answers = so.answers(user_id=answerer, sort='creation',
                    pagesize=100)
            # save answers to queue
            answer_q = coll.deque(answers)
        else:
            # answer queue is restored
            restored = True

        # save state
        save_state(graph, node_q, answer_q, answerer, restored)

        while len(answer_q) > 0:
            try:
                # if answer queue is restored need to fetch full data, else it is
                # already available
                if restored:
                    _id = answer_q.popleft()
                    a = so.answer(_id, sort='creation', filter='default',
                            body=True)
                else:
                    a = answer_q.popleft()

                # retrieve owner of question to which the answer is tied
                q_owner = a.question.owner
                questioner = q_owner.id

                # if questioner is present do nothing, else update its user details
                if questioner in graph:
                    # in a normal situazione would add, but not today
                    # no need to lower min positive degree to zero
                    #graph.add_node(questioner,
                    #        reputation = q_owner.reputation,
                    #        name = q_owner.display_name,
                    #        link = q_owner.url
                    #        )

                    # if an edge answerer -> questioner exist update weight +1
                    # else create it with weight 1
                    if graph.has_edge(answerer, questioner):
                        graph[answerer][questioner]['weight'] += 1
                    else:
                        graph.add_edge(answerer, questioner, weight=1)
                
                # save state for every edge added
                save_state(graph, node_q, answer_q, answerer, restored)
            except Exception as e:
                # print exception message
                error(e)
                # close immediately if a 502 (api requests emptied)
                if hasattr(e, 'code') and e.code == 502:
                    print('Run out of API requests, exiting...')
                    sys.exit(255)

        # save state for every answerer completed
        save_state(graph, node_q, None, None, True)
    except Exception as e:
        # print exception message
        error(e)
        # close immediately if a 502 (api requests emptied)
        if hasattr(e, 'code') and e.code == 502:
            print('Run out of API requests, exiting...')
            sys.exit(255)
