import requests, time
import html as shtml
import networkx as nx
from lxml import html

igraph = nx.read_gpickle('igraph.pkl')

excp_count = 0
def get_rank(name, id_):
    global excp_count
    
    T_URL = 'https://stackexchange.com/leagues/filter-users/1/alltime/?filter='
    URL = '{}{}'.format(T_URL, name)
    user_agent = {
        'User-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 \
        (KHTML, like Gecko) Chrome/58.0.3029.97 Safari/537.36 \
        Vivaldi/1.9.818.49'
    }
    page = requests.get(URL, headers=user_agent).content
    tree = html.fromstring(page)

    profile = tree.xpath('//div/a/@name')
    hashranks = tree.xpath('//div/div[2]/div[1]/span/text()')
    
    hashrank =  False
    for x,y in zip(profile, hashranks):
        if x == str(id_):
            hashrank = y

    if hashrank:
        rank = int(hashranks[0][1:])
        return rank
    else:
        excp_count += 1
        return -1

for i in igraph:
    node = igraph.node[i]
    name = node['name']

    dec_name = shtml.unescape(name)
    if name != dec_name:
        node['name'] = dec_name
        name = node['name']

    print('{} {} {}'.format('-' * 40, name, i))
    rank = get_rank(name, i)
    if rank == -1: print('!'*80)
    print(f'rank: {rank}')
    node['rank'] = rank

    time.sleep(1.0/3.0)

print(f'-1: {excp_count}')
input('Press enter to continue...')

nx.write_gpickle(igraph, 'ranked-graph.pkl')
