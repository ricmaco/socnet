import networkx as nx

graph = nx.read_gpickle('graph.pkl')

for i,j,k in graph.edges(data=True):
    k['weight'] = 1 / float(k['weight'])

pr = nx.pagerank(graph)
nw = nx.pagerank(graph, weight=None)

ipr = {v: k for k, v in pr.items()}
inw = {v: k for k, v in nw.items()}

w = sorted(ipr)[-10:]
n = sorted(inw)[-10:]

setw = set()
setn = set()

print('Weighted', 'Normal')
for x,y in zip(w, n):
    setw.add(ipr[x])
    setn.add(inw[y])
    print(x, y)
    print(ipr[x], inw[y])
    print()

for x,y in zip(setw, setn):
    print(x, y)

input('Press enter to save graph...')

nx.write_gpickle(graph, 'revweigth-graph.pkl')
