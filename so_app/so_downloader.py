#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pickle, os, sys
import collections as coll
import traceback as tb
from os import path

import networkx as nx

import stackexchange
# some fix for library
from stackexchange.models import Question
Question.__eq__ = lambda self,x: x.id == self.id

# max number of requests possible
MAX_REQUESTS = 10000
# max number of nodes to retrieve
MAX_NODES = 30000

# we will start our traversal from Jon Skeet
JON_SKEET = 22656

# https://stackapps.com/apps/oauth/view/9741
API_KEY = 'ekaG*7C8*io7TlLGtFUFRw(('

# debug
DEBUG = True

# folder to contain saves
save_folder = 'saves'
os.makedirs(save_folder, exist_ok=True)
# name of graph file
graph_filename = 'graph.pkl'
# name of node queue file
nq_filename = 'node_queue.pkl'
# name of question queue
qq_filename = 'question_queue.pkl'

# default cache to 3 hours
so = stackexchange.Site(stackexchange.StackOverflow, API_KEY, cache=10800)

# make library deal with SO throttling, waiting 30req/5s
so.impose_throttling = True
so.throttle_stop = False

# debug
#stackexchange.web.WebRequestManager.debug = True

# append filenames to save folder
g_filename = path.join(save_folder, graph_filename)
nq_filename = path.join(save_folder, nq_filename)
qq_filename = path.join(save_folder, qq_filename)

def debug(message, graph, type='debug'):
    '''Prints a debug message with header and footer, if debug variable is set.

    Args:
        message (str): message to print.
        graph (networkx.Graph): graph for printing sizes.
        type (str): message type.

    '''
    types = {
        'debug': {
            'char': '$',
            'level': 5
        },
        'node-start': {
            'char': '#',
            'level': 60
        },
        'node-end': {
            'char': '#',
            'level': 60
        },
        'question-start': {
            'char': '#',
            'level': 30
        },
        'question-end': {
            'char': '#',
            'level': 30
        },
        'answer': {
            'char': '-',
            'level': 1
        },
        'exception': {
            'char': '!',
            'level': 20
        },
        'save': {
            'char': '%',
            'level': 10
        }
    }

    if DEBUG:
        graph = 'Nodes: {} - Edges: {}'.format(len(graph), graph.size())

        if type in ('debug', 'exception', 'save'):
            print(types[type]['char'] * types[type]['level'], type.upper())
            print(graph)
            print('-' * types[type]['level'])
            print(message)
            print(types[type]['char'] * types[type]['level'])
        elif type in ('node-start', 'question-start'):
            print(types[type]['char'] * types[type]['level'], message)
            print(graph)
        elif type in ('node-end', 'question-end'):
            print(types[type]['char'] * types[type]['level'])
        elif type in ('answer'):
            print(types[type]['char'], message)
    
def save_state(graph, questioner, node_queue, question_queue, restored=False,
        copy_queue=True):
    '''Pickles provided graph and queue to their respective paths.

    Args:
        graph (networkx.Graph): graph to save to file.
        questioner (str): id of questioner.
        node_queue (obj): node queue to save to file.
        question_queue (obj): question queue to save to file.
        restored (bool): wheter question queue is restored or created.
        copy_queue (bool): wheter to produce a copy with current questioner of
            node queue or not.

    '''
    debug('Saving state...', graph, 'save')

    # save state (and reinsert questioner into node_queue)
    if copy_queue:
        save_node_queue = node_queue.copy()
        save_node_queue.append(questioner)
    else:
        save_node_queue = node_queue
    # if question queue is created save only ids
    if not restored:
        save_question_queue = coll.deque([q.id for q in question_queue])
    else:
        save_question_queue = question_queue

    # save graph
    nx.write_gpickle(graph, g_filename)
    # save node queue
    with open(nq_filename, 'wb') as file:
        pickle.dump(save_node_queue, file)
    # save question queue
    with open(qq_filename, 'wb') as file:
        pickle.dump(save_question_queue, file)

# create or open a graph
if path.isfile(g_filename):
    so_graph = nx.read_gpickle(g_filename)
else:
    so_graph = nx.DiGraph(name='Stack Overflow')

# decided to start from the first in rank, Jon Skeet
if path.isfile(nq_filename):
    with open(nq_filename, 'rb') as file:
        node_queue = pickle.load(file)
else:
    # open saved node queue
    node_queue = coll.deque((JON_SKEET,))

# create or open a question queue
if path.isfile(qq_filename):
    with open(qq_filename, 'rb') as file:
        question_queue = pickle.load(file)
else:
    question_queue = coll.deque()

while len(so_graph) < MAX_NODES and len(node_queue) > 0:
    try:
        # retrieve current questioner and try to add to graph
        questioner = node_queue.popleft()

        debug(questioner, so_graph, 'node-start')

        # test for presence
        presence = questioner in so_graph

        # if there is not a question queue, download questions and create one
        # else question queue is already loaded
        if not question_queue or len(question_queue) == 0:
            # signal it is created, not restored
            restored = False
            
            # fetch questions for current questioner
            # q_ will be for 'questioner'
            q_questions = so.questions(user_id=questioner, sort='creation')
            # save questions to question queue
            question_queue = coll.deque(q_questions)

            # if node is new:
            #   1. add node to graph
            #   2. retrieve user data from questions (or a request if no
            #      questions  present)
            if not presence:
                # 1
                so_graph.add_node(questioner)
                # 2
                if len(q_questions) > 0:
                    q_owner = q_questions[0].owner
                    so_graph.add_node(questioner,
                            reputation = q_owner.reputation,
                            name = q_owner.display_name,
                            link = q_owner.link#,
                            #questions = {},
                            #answers = {}
                            )
                else:
                    # TODO: else make a request for user data
                    pass
        # else question queue will be made only of question ids, so later it is
        # required to take care of this (e.g. retrieve full question)
        else:
            # signal it is restored, not created
            restored = True

        # save_state
        save_state(so_graph, questioner, node_queue, question_queue, restored)

        # for every question in question queue
        while len(question_queue) > 0:
            # create a shortcut for questioner's question structure
            #q_attr_questions = so_graph.node[questioner]['questions']

            # if restored make request to retrieve full question
            if restored:
                _id = question_queue.popleft()
                q = so.question(_id)
            # else directly pop from queue
            else:
                q = question_queue.popleft()

            debug(q.id, so_graph, 'question-start')

            # save answers dict for later
            q_answers = {}
            # populate questioner's question structure
            #q_attr_questions[q.id] = {
            #        'is_answered': q.is_answered,
            #        'answer_count': q.answer_count,
            #        'score': q.score,
            #        'creation_date': q.creation_date,
            #        'link': q.link,
            #        'title': q.title,
            #        'answers': q_answers
            #        }

            # for every answered question create an edge between questioner and
            # answerer
            # edge weight is the number of times an answerer answered to a
            # questioner question
            if q.is_answered:
                for a in q.answers:      
                    try:
                        debug(a.id, so_graph, 'answer')

                        # retrieve answerer data, add it to node queue and
                        # check if it already is in the graph
                        # a_ will be for 'answerer'
                        a_owner = a.owner
                        answerer = a_owner.id

                        # test for presence, for later use
                        presence = answerer in so_graph

                        # if node is not already into node_queue and not into
                        # graph, append it to queue
                        if not presence and answerer not in node_queue:
                            node_queue.append(answerer)

                        # if node is new:
                        #   1. create answerer node with data
                        #   2. create edge with weight 1
                        if not presence:
                            # 1
                            so_graph.add_node(answerer,
                                    reputation = a_owner.reputation,
                                    name = a_owner.display_name,
                                    link = a_owner.url#,
                                    #questions = {},
                                    #answers = {}
                                    )
                            # 2
                            so_graph.add_edge(answerer, questioner, weight=1)
                        # if node is already present
                        #   1. update edge weight to weight+1 (or if first-time
                        #   loop create edge with weight 1)
                        else:
                            # 1
                                if so_graph.number_of_edges(answerer, questioner) == 0:
                                    so_graph.add_edge(answerer, questioner, weight=1)
                                else:
                                    so_graph[answerer][questioner]['weight'] += 1

                        # update answers dict
                        #a_answers = so_graph.node[answerer]['answers']
                        #a_answers[a.id] = {
                        #        'is_accepted': a.is_accepted,
                        #        'score': a.score,
                        #        'question_id': q.id,
                        #        'creation_date': a.creation_date,
                        #        'link': q.link
                        #        }

                        # add answer to question
                        #q_answers[a.id] = a_answers[a.id]
                    except Exception as e:
                        debug(e.name, so_graph, 'exception')
                        # close immediately if a 502 (api requests emptied)
                        if e.code == 502:
                            print('Run out of API requests, exiting...')
                            sys.exit(255)

                # save state
                save_state(so_graph, questioner, node_queue, question_queue,
                        restored)

            debug('', so_graph, 'question-end')

        ### END OF GINORMOUS WHILE

        # save state
        save_state(so_graph, None, node_queue, None, True, False)

        debug('', so_graph, 'node-end')
    except Exception as e:
        debug(e.name, so_graph, 'exception')
        # close immediately if a 502 (api requests emptied)
        if e.code == 502:
            print('Run out of API requests, exiting...')
            sys.exit(255)
